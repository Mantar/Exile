------------------------------------------------------------
--CLOTHING init
------------------------------------------------------------

-- Internationalization
local S = minetest.get_translator("clothing")
local FS = function(...)
    return minetest.formspec_escape(S(...))
end

local modpath = minetest.get_modpath(minetest.get_current_modname())

dofile(modpath.."/api.lua")
--dofile(modpath.."/test_clothing.lua") --bug testing


--Not available except through creative
minetest.override_item("player_api:cloth_unisex_footwear_default", {
			  temp_min = 50,
			  temp_max = 50,
			  adminclothes = true,
})

------------------------------------------------------------
-- Inventory page

local clothing_formspec = "size[8,8.5]"..
"list[current_player;main;0,4.7;8,1;]"..
"list[current_player;main;0,5.85;8,3;8]"

sfinv.register_page("clothing:clothing", {
	title = S("Clothing"),
	get = function(self, player, context)
		local meta = player:get_meta()
		local cur_tmin = climate.get_temp_string(meta:get_int("clothing_temp_min"), meta)
		local cur_tmax = climate.get_temp_string(meta:get_int("clothing_temp_max"), meta)
		local basetex = minetest.formspec_escape(
		   player_api.get_current_texture(player) )

		local formspec = clothing_formspec..
		"label[3,0.4;" .. FS("Min Temperature Tolerance: @1", cur_tmin) .. " ]"..
		"label[3,1;" .. FS("Max Temperature Tolerance: @1", cur_tmax) .. " ]"..
		--"list[detached:"..name.."_clothing;clothing;0,0.5;2,3;]"..
		"list[current_player;cloths;0,0.5;2,3;]" ..
		"listring[current_player;main]"..
		--"listring[detached:"..name.."_clothing;clothing]"
		"listring[current_player;cloths]"..
		"model[6.5,2;2,3;character;character.b3d;"..basetex..
		   ";-20,160;;true;;]"
		return sfinv.make_formspec(player, context,
		formspec, false)
	end
})

minetest.register_on_player_inventory_action(function(player, action, inventory, inventory_info)
      if inventory_info.to_list == "cloths" or inventory_info.from_list == "cloths" then
	 player_api.set_texture(player)
	 clothing:update_temp(player)
      end
end)

minetest.register_allow_player_inventory_action(function(player, action, inventory, inventory_info)
	local stack, from_inv, to_index
	if action == "move" and inventory_info.to_list == "cloths" then
		if inventory_info.from_list == inventory_info.to_list then --for moving inside the 'cloths' inventory
		   return 1
		end
		--for moving items from player inventory list 'main' to 'cloths'
		from_inv = "main"
		to_index = inventory_info.to_index
		stack = inventory:get_stack(inventory_info.from_list, inventory_info.from_index)
	elseif action == "put" and inventory_info.listname == "cloths" then
		--for moving from node inventory 'closet' to player inventory 'cloths'
		from_inv = "closet"
		to_index = inventory_info.index
		stack = inventory_info.stack
	else -- we're taking something out or doing some unrelated inv action
	   return
	end
	if stack then
		local stack_name = stack:get_name()
		local item_group = minetest.get_item_group(stack_name , "cloth")
		if item_group == 0 --not a cloth
		 or item_group == 6 then -- or it's a blanket
			return 0
		end
		--search for another cloth of the same type
		local player_inv = player:get_inventory()
		local cloth_list = player_inv:get_list("cloths")
		for i = 1, #cloth_list do
			local cloth_name = cloth_list[i]:get_name()
			local cloth_type = minetest.get_item_group(cloth_name, "cloth")
			if cloth_type == item_group then
			   if from_inv == "main" then
			      local removed = player_inv:remove_item("cloths", cloth_name)
			      if player_inv:room_for_item("main", removed) then
				 player_inv:add_item("main", removed)
				 return 1
			      else
				 minetest.item_drop(removed, player, player:get_pos())
			      end
			   end
			end
		end
		return 1
	end
	return 0
end)

--functions


local function load_clothing_metadata(player)
   -- Exile clothing was stored as a metadata string, migrate to new inv
	local player_inv = player:get_inventory()
	local meta = player:get_meta()
	local clothing_meta = meta:get_string("clothing:inventory")
	local clothes = clothing_meta and minetest.deserialize(clothing_meta) or {}
	if clothing_meta == "" then
	   return
	end
	-- Fill detached slots
	--clothing_inv:set_size("clothing", 6)
	for i = 1, 6 do
	   player_inv:set_stack("cloths", i, clothes[i] or "")
	   --overwrite current clothes, but it will be empty on first migration
	end
	meta:set_string("clothing:inventory", "")
end

minetest.register_on_joinplayer(function(player)
      --import old clothing
      load_clothing_metadata(player)
      player_api.set_texture(player)
      clothing:update_temp(player)
end)
