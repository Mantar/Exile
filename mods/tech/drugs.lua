------------------------------------
--DRUGS
--medicines etc

-----------------------------------

-- Internationalization
local S = tech.S

local random = math.random

-----------------------------------
--MEDICAL


------------
--Herbal medicine
-- removes energy cost of plants healing effects
--can heal certain health effects
--a restorative anti-bacterial/anti-parasitic
minetest.register_craftitem("tech:herbal_medicine", {
	description = S("Herbal Medicine"),
	inventory_image = "tech_herbal_medicine.png",
	stack_max = minimal.stack_max_medium *2,
	groups = {flammable = 1},

  on_use = function(itemstack, user, pointed_thing)
    local meta = user:get_meta()
    local effects_list = meta:get_string("effects_list")
    effects_list = minetest.deserialize(effects_list) or {}

    --remove parasites
    if random()<0.33 then
  		HEALTH.remove_new_effect(user, {"Intestinal Parasites"})
  	end

    --cure/reduce food poisoning and infections
    --see how effective the dose is
    local cfp = random()
    if cfp <0.25 then
      --cure up to severe
      HEALTH.remove_new_effect(user, {"Food Poisoning", 3})
			HEALTH.remove_new_effect(user, {"Fungal Infection", 3})
			HEALTH.remove_new_effect(user, {"Dust Fever", 3})
    elseif cfp < 0.5 then
      --cure up to moderate
      HEALTH.remove_new_effect(user, {"Food Poisoning", 2})
			HEALTH.remove_new_effect(user, {"Fungal Infection", 2})
			HEALTH.remove_new_effect(user, {"Dust Fever", 2})
    elseif cfp < 0.75 then
      --only cure mild
      HEALTH.remove_new_effect(user, {"Food Poisoning", 1})
			HEALTH.remove_new_effect(user, {"Fungal Infection", 1})
			HEALTH.remove_new_effect(user, {"Dust Fever", 1})
    end


    --hp_change, thirst_change, hunger_change, energy_change, temp_change, replace_with_item
    return HEALTH.use_item(itemstack, user, 5, 0, 0, 0, 0)
  end,
})


------------
--Detox?
--for toxins, alcohol, drugs
-- e.g. charcoal? - doesn't work for everything, can itself cause vomiting etc

--[[
How toxins get treated:
Alcohol: wait for it to pass (while helping them not die), pump stomach
Stimulant OD: sedation, wait for it to pass (while helping them not die)
Specific toxins can have anti-toxins (a fairly modern treatment)

]]


-----------------------------------
-----------------------------------
--DRUGS

-----------------------------------
--STIMULANTS


------------
--Tiku
-- stimulant drug
-- gets you high
minetest.register_craftitem("tech:tiku", {
	description = S("Tiku (stimulant)"),
	inventory_image = "tech_tiku.png",
	stack_max = minimal.stack_max_medium *2,
	groups = {flammable = 1, drug = 1},

  on_use = function(itemstack, user, pointed_thing)

    --begin the bender
		HEALTH.add_new_effect(user, {"Tiku High", 1})

    --hp_change, thirst_change, hunger_change, energy_change, temp_change, replace_with_item
    return HEALTH.use_item(itemstack, user, 0, 0, -24, 96, 0)
  end,
})



-----------------------------------
--DEPRESSANTS


-----------------
--Tang, alcoholic drink

--Pot of Tang
liquid_store.register_stored_liquid(
  "tech:tang_liquid",
	"tech:tang",
	"tech:clay_water_pot",
	{
		"tech_pottery.png^tech_pot_empty.png^tech_pot_tang.png",
		"tech_pottery.png",
		"tech_pottery.png",
		"tech_pottery.png",
		"tech_pottery.png",
		"tech_pottery.png"
	},
  {
		type = "fixed",
		fixed = {
			{-0.25, 0.375, -0.25, 0.25, 0.5, 0.25}, -- NodeBox1
			{-0.375, -0.25, -0.375, 0.375, 0.3125, 0.375}, -- NodeBox2
			{-0.3125, -0.375, -0.3125, 0.3125, -0.25, 0.3125}, -- NodeBox3
			{-0.25, -0.5, -0.25, 0.25, -0.375, 0.25}, -- NodeBox4
			{-0.3125, 0.3125, -0.3125, 0.3125, 0.375, 0.3125}, -- NodeBox5
		}
	},
  S("Tang"),
	{dig_immediate=2, pottery = 1, temp_pass = 1, drug = 1})
liquid_store.register_stored_liquid(
  "tech:tang_liquid",
	"tech:wooden_tang",
	"tech:wooden_water_pot",
	{
		"tech_primitive_wood.png^tech_pot_empty.png^tech_pot_tang.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png"
	},
  {
		type = "fixed",
		fixed = {
			{-0.25, 0.375, -0.25, 0.25, 0.5, 0.25}, -- NodeBox1
			{-0.375, -0.25, -0.375, 0.375, 0.3125, 0.375}, -- NodeBox2
			{-0.3125, -0.375, -0.3125, 0.3125, -0.25, 0.3125}, -- NodeBox3
			{-0.25, -0.5, -0.25, 0.25, -0.375, 0.25}, -- NodeBox4
			{-0.3125, 0.3125, -0.3125, 0.3125, 0.375, 0.3125}, -- NodeBox5
		}
	},
  S("Tang"),
	{dig_immediate=2, temp_pass = 1, drug = 1})

local function drink_tang(pot_type, pos, node, clicker, itemstack, pointed_thing)
  if (pot_type ~= "wooden") then
    pot_type = "clay"
  end
  --lets skull an entire vat of booze, what could possibly go wrong...
  local meta = clicker:get_meta()
  local thirst = meta:get_int("thirst")
  local hunger = meta:get_int("hunger")
  local energy = meta:get_int("energy")
  --only drink if thirsty
  if thirst < 100 then

    --you're skulling a whole bucket
    thirst = 100 -- no point in adding to if this is the maximum

    --all energy and half food equivalent of the fruit
    --gets given as energy
    energy = minimal.math_clamp(energy + 180, 1000, 0)
    hunger = minimal.math_clamp(hunger + 60, 1000, 0)

    --drunkness
    if random() < 0.75 then
      HEALTH.add_new_effect(clicker, {"Drunk", 1})
    end


    meta:set_int("thirst", thirst)
    meta:set_int("energy", energy)
    meta:set_int("hunger", hunger)

    minetest.swap_node(pos, {name = "tech:"..pot_type.."_water_pot"})
    minetest.sound_play("nodes_nature_slurp",	{pos = pos, max_hear_distance = 3, gain = 0.25})
  end
end

-- overrides for clay and wooden tang
minetest.override_item("tech:tang",{
  on_rightclick = function(...)
    drink_tang("",...)
  end
})
minetest.override_item("tech:wooden_tang",{
  on_rightclick = function(...)
    drink_tang("wooden",...)
  end
})
-------------

--Pot of new Tang, must be left to ferment
liquid_store.register_stored_liquid(
  "tech:unfermented_tang_liquid",
	"tech:tang_unfermented",
	"tech:clay_water_pot",
	{
		"tech_pottery.png^tech_pot_empty.png^tech_pot_tang_uf.png",
		"tech_pottery.png",
		"tech_pottery.png",
		"tech_pottery.png",
		"tech_pottery.png",
		"tech_pottery.png"
	},
  {
		type = "fixed",
		fixed = {
			{-0.25, 0.375, -0.25, 0.25, 0.5, 0.25}, -- NodeBox1
			{-0.375, -0.25, -0.375, 0.375, 0.3125, 0.375}, -- NodeBox2
			{-0.3125, -0.375, -0.3125, 0.3125, -0.25, 0.3125}, -- NodeBox3
			{-0.25, -0.5, -0.25, 0.25, -0.375, 0.25}, -- NodeBox4
			{-0.3125, 0.3125, -0.3125, 0.3125, 0.375, 0.3125}, -- NodeBox5
		}
	},
  S("Tang (unfermented)"),
	{dig_immediate=2, pottery = 1, temp_pass = 1})
-- wooden pot of unfermented tang
liquid_store.register_stored_liquid(
  "tech:unfermented_tang_liquid",
	"tech:wooden_tang_unfermented",
	"tech:wooden_water_pot",
	{
		"tech_primitive_wood.png^tech_pot_empty.png^tech_pot_tang_uf.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png",
		"tech_primitive_wood.png"
	},
  {
		type = "fixed",
		fixed = {
			{-0.25, 0.375, -0.25, 0.25, 0.5, 0.25}, -- NodeBox1
			{-0.375, -0.25, -0.375, 0.375, 0.3125, 0.375}, -- NodeBox2
			{-0.3125, -0.375, -0.3125, 0.3125, -0.25, 0.3125}, -- NodeBox3
			{-0.25, -0.5, -0.25, 0.25, -0.375, 0.25}, -- NodeBox4
			{-0.3125, 0.3125, -0.3125, 0.3125, 0.375, 0.3125}, -- NodeBox5
		}
	},
  S("Tang (unfermented)"),
	{dig_immediate=2, temp_pass = 1})

-- find ferment or create a ferment meta
local function get_or_create_ferment(meta)
  local ferment = meta:get_int("ferment")
  
  if (ferment == 0) then
    ferment = math.random(300,360)
  end
  
  return ferment
end

--save usage into inventory, to prevent infinite supply
local on_dig_tang = function(pos, node, digger, pot_type)
	if minetest.is_protected(pos, digger:get_player_name()) then
		return false
	end
  
  if (type(pot_type) == "string") then
    pot_type = string.lower(pot_type)
  else
    pot_type = ""
  end
  

	local meta = minetest.get_meta(pos)
	local ferment = get_or_create_ferment(meta)

	local new_stack = ItemStack("tech:tang_unfermented")
  if (string.match(pot_type,"wooden")) then
    new_stack = ItemStack("tech:wooden_tang_unfermented")
  end
	local stack_meta = new_stack:get_meta()
	stack_meta:set_int("ferment", ferment)


	minetest.remove_node(pos)
	local player_inv = digger:get_inventory()
	if player_inv:room_for_item("main", new_stack) then
		player_inv:add_item("main", new_stack)
	else
		minetest.add_item(pos, new_stack)
	end
end

--set saved
local after_place_tang = function(pos, placer, itemstack, pointed_thing)
	local meta = minetest.get_meta(pos)
	local stack_meta = itemstack:get_meta()
	local ferment = get_or_create_ferment(stack_meta)
	if ferment >0 then
		meta:set_int("ferment", ferment)
	end
end

local on_construct_tang = function(pos)
  --duration of ferment
		local meta = minetest.get_meta(pos)
    meta:set_int("ferment", math.random(300,360))
		--ferment
		minetest.get_node_timer(pos):start(5)
end

-- custom function that preserves metadata from a replaced node to an itemstack
local preserve_metadata_tang = function(pos, oldnode, oldmeta, transferred_stack)
  local imeta = transferred_stack:get_meta()
  
  imeta:set_int("ferment",get_or_create_ferment(oldmeta))
end

-- function overrides for unfermented tang
minetest.override_item("tech:tang_unfermented",{
  on_dig = function(pos, node, digger)
		on_dig_tang(pos, node, digger)
	end,

	on_construct = function(pos)
		on_construct_tang(pos)
	end,

	after_place_node = function(pos, placer, itemstack, pointed_thing)
		after_place_tang(pos, placer, itemstack, pointed_thing)
	end,

	on_timer = function(pos, elapsed)
		local meta = minetest.get_meta(pos)
		local ferment = meta:get_int("ferment")
		if ferment < 1 then
			minetest.swap_node(pos, {name = "tech:tang"})
			--minetest.check_for_falling(pos)
			return false
		else
      --ferment if at right temp
      local temp = climate.get_point_temp(pos)
      if temp > 10 and temp < 34 then
        meta:set_int("ferment", ferment - 1)
      end
			return true
		end
	end,
  
  _preserve_metadata = function(...)
    preserve_metadata_tang(...)
  end,
})
minetest.override_item("tech:wooden_tang_unfermented",{
  on_dig = function(pos, node, digger)
		on_dig_tang(pos, node, digger,"wooden")
	end,

	on_construct = function(pos)
		on_construct_tang(pos)
	end,

	after_place_node = function(pos, placer, itemstack, pointed_thing)
		after_place_tang(pos, placer, itemstack, pointed_thing)
	end,

	on_timer = function(pos, elapsed)
		local meta = minetest.get_meta(pos)
		local ferment = meta:get_int("ferment")
		if ferment < 1 then
			minetest.swap_node(pos, {name = "tech:wooden_tang"})
			--minetest.check_for_falling(pos)
			return false
		else
      --ferment if at right temp
      local temp = climate.get_point_temp(pos)
      if temp > 10 and temp < 34 then
        meta:set_int("ferment", ferment - 1)
      end
			return true
		end
	end,
  
  _preserve_metadata = function(...)
    preserve_metadata_tang(...)
  end,
})

-----------------------------------
--HALLUCINOGENS



---------------------------------------
--Recipes



--
--mortar and pestle
--


--make herbal_medicine
crafting.register_recipe({
	type = "mortar_and_pestle",
	output = "tech:herbal_medicine",
	items = {'nodes_nature:hakimi', 'nodes_nature:merki', 'nodes_nature:moss'},
	level = 1,
	always_known = true,
})

--make tiku
crafting.register_recipe({
	type = "mortar_and_pestle",
	output = "tech:tiku",
	items = {'nodes_nature:tikusati_seed 12', 'nodes_nature:wiha', "tech:vegetable_oil"},
	level = 1,
	always_known = true,
})

--make tang_unfermented
crafting.register_recipe({
	type = "mortar_and_pestle",
	output = "tech:tang_unfermented",
	items = {'nodes_nature:tangkal_fruit 12', "tech:clay_water_pot_freshwater"},
	level = 1,
	always_known = true,
})
-- make tang unfermented in wooden pot
crafting.register_recipe({
	type = "mortar_and_pestle",
	output = "tech:wooden_tang_unfermented",
	items = {'nodes_nature:tangkal_fruit 12', "tech:wooden_water_pot_freshwater"},
	level = 1,
	always_known = true,
})
